const formSchema = {
  firstName: {
    required: true,
    validator: {
      regEx: /^[a-zA-Z _-]+$/,
      error: 'Invalid first name format.'
    }
  },
  lastName: {
    required: true,
    validator: {
      regEx: /^[a-zA-Z _-]+$/,
      error: 'Invalid last name format.'
    }
  },
  email: {
    required: true,
    validator: {
      regEx: /^[^@]+@[^@]+\.[^@]+$/,
      error: 'Invalid email format.'
    }
  },
  mobile: {
    required: false,
    validator: {
      regEx: /^04[0-9]{8}$/,
      error: 'Invalid mobile format.'
    }
  }
};

export default formSchema;
