import React from "react";
import { shallow } from "enzyme";
import RegisterForm from "./RegisterForm";

describe("RegisterForm component", () => {
  let wrapper;
  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, "useState");
  useStateSpy.mockImplementation(init => [init, setState]);

  const postData = jest.fn();
  const isSubmitting = false;
  const setIsSubmitting = jest.fn();

  beforeEach(() => {
    wrapper = shallow(
      <RegisterForm
        postData={postData}
        isSubmitting={isSubmitting}
        setIsSubmitting={setIsSubmitting}
      />
    );
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("shallow renders without crashing", () => {
    shallow(
      <RegisterForm
        postData={postData}
        isSubmitting={isSubmitting}
        setIsSubmitting={setIsSubmitting}
      />
    );
  });

  it("render snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("calls setStat when change the input", () => {
    const input = wrapper.find('input[name="lastName"]');
    input.simulate("focus");
    input.simulate("change", { target: { name: "lastName", value: "chien" } });
    expect(setState).toHaveBeenCalledWith(true);
  });
});
