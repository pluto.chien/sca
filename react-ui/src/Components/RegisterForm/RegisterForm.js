import React from 'react';
import PropTypes from 'prop-types';
import loading from '../../icons/loading.svg';
import formValidation from '../../helper';
import formSchema from '../../schema';

const RegisterForm = ({ postData, isSubmitting, setIsSubmitting }) => {
  const stateSchema = {
    firstName: { value: '', error: '' },
    lastName: { value: '', error: '' },
    email: { value: '', error: '' },
    mobile: { value: '', error: '' }
  };

  function onSubmitForm(state) {
    setIsSubmitting(true);
    postData({
      firstName: state.firstName.value,
      lastName: state.lastName.value,
      email: state.email.value,
      mobile: state.mobile.value
    });
  }

  const {
    formData: {
      firstName,
      lastName,
      email,
      mobile
    },
    handleOnChange,
    handleOnSubmit,
    disable
  } = formValidation(stateSchema, formSchema, onSubmitForm);

  return (
    <form onSubmit={handleOnSubmit} noValidate>
      <div className="field">
        <label htmlFor="firstName">
          First Name
          <span className="required" aria-hidden="true">
            *
          </span>
        </label>
        <input
          type="text"
          name="firstName"
          className={firstName.error ? 'error' : ''}
          aria-describedby="first name is required"
          value={firstName.value}
          onChange={handleOnChange}
        />
        {firstName.error && <p className="error">{firstName.error}</p>}
      </div>
      <div className="field">
        <label htmlFor="lastName">
          Last Name
          <span className="required" aria-hidden="true">
            *
          </span>
        </label>
        <input
          type="text"
          name="lastName"
          className={lastName.error ? 'error' : ''}
          aria-describedby="last name is required"
          value={lastName.value}
          onChange={handleOnChange}
        />
        {lastName.error && <p className="error">{lastName.error}</p>}
      </div>
      <div className="field">
        <label htmlFor="email">
          Email
          <span className="required" aria-hidden="true">
            *
          </span>
        </label>
        <input
          type="email"
          name="email"
          className={email.error ? 'error' : ''}
          aria-describedby="email is required"
          value={email.value}
          onChange={handleOnChange}
        />
        {email.error && <p className="error">{email.error}</p>}
      </div>
      <div className="field">
        <label htmlFor="mobile">Mobile Phone</label>
        <input
          type="text"
          name="mobile"
          className={mobile.error ? 'error' : ''}
          aria-describedby="mobile is optional"
          value={mobile.value}
          onChange={handleOnChange}
        />
        {mobile.error && <p className="error">{mobile.error}</p>}
      </div>
      <div className="submit">
        <input
          type="submit"
          name="submit"
          value={isSubmitting ? 'Submitting' : 'submit'}
          className={disable ? 'button disabled' : 'button'}
          disabled={disable}
        />
        {isSubmitting ? <img alt="loading" src={loading} /> : null}
      </div>
    </form>
  );
};

RegisterForm.propTypes = {
  postData: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  setIsSubmitting: PropTypes.func.isRequired
};
export default RegisterForm;
