import React from "react";
import { shallow } from "enzyme";
import Results from "./Results";

describe("RegisterForm component", () => {
  let wrapper;

  const isSuccess = false;

  beforeEach(() => {
    wrapper = shallow(<Results isSuccess={isSuccess} />);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("shallow renders without crashing", () => {
    shallow(<Results isSuccess={isSuccess} />);
  });

  it("render snapshot when isSuccess is false", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("render snapshot when isSuccess is true", () => {
    expect(<Results isSuccess={true} />).toMatchSnapshot();
  });
});
