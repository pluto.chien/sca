import React from 'react';
import PropTypes from 'prop-types';
import './Results.css';

const Results = ({ isSuccess }) => {
  const message = isSuccess
    ? 'Your information has been submitted successfully.'
    : 'The application has encountered an unknown error.';
  return <div className={isSuccess ? 'success' : 'failed'}>{message}</div>;
};

Results.propTypes = {
  isSuccess: PropTypes.bool.isRequired
};
export default Results;
