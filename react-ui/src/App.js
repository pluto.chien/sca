import React, { useState } from 'react';
import axios from 'axios';
import './App.css';
import Results from './Components/Results';
import RegisterForm from './Components/RegisterForm';

function App() {
  const [isSuccessPost, setIsSuccessPost] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  const postData = (data) => {
    axios.post('/signup', JSON.stringify(data), {headers})
        .then((response) => {
          console.log(response);
          setIsSuccessPost(true);
          setIsSubmitting(false);
        }, (error) => {
          console.log(error);
          setIsSuccessPost(false);
          setIsSubmitting(false);
        });
  }

  return (
    <div className="App">
      <header>Subscribe</header>
      <div className="wrapper">
        <div className="form-wrapper">
          <div className="modal-header">Register</div>
          {isSuccessPost != null ? (
            <Results isSuccess={isSuccessPost} />
          ) : (
            <RegisterForm
              postData={postData}
              isSubmitting={isSubmitting}
              setIsSubmitting={setIsSubmitting}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
