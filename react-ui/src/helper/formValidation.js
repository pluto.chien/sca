import { useState, useEffect, useCallback } from 'react';

function formValidation(defaultFormData, fromSchema = {}, callback) {
  const [formData, setFormData] = useState(defaultFormData);
  const [disable, setDisable] = useState(true);
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    setDisable(true);
  }, []);

  const validateFormData = useCallback(() => Object.keys(fromSchema).some((key) => {
      const isInputFieldRequired = fromSchema[key].required;
      const inputValue = formData[key].value;
      const inputError = formData[key].error;

      return (isInputFieldRequired && !inputValue) || inputError;
    }), [formData, fromSchema]);

    useEffect(() => {
        if (isDirty) {
            setDisable(validateFormData());
        }
    }, [formData, isDirty]);

    const handleOnChange = useCallback(
      (event) => {
      setIsDirty(true);
      const { name, value } = event.target;
      let errorString = '';
      if (fromSchema[name].required && !value) {
          errorString = 'This is required field.';
      }

      if (value && fromSchema[name].validator && !fromSchema[name].validator.regEx.test(value)) {
            errorString = fromSchema[name].validator.error;
        }

      setFormData(prevState => ({
        ...prevState,
        [name]: { value, error: errorString }
      }));
    },
    [fromSchema]
  );

  const handleOnSubmit = useCallback(
      (event) => {
      event.preventDefault();

      if (!validateFormData()) {
        callback(formData);
      }
    },
    [formData]
  );

  return {
      formData,
      disable,
      handleOnChange,
      handleOnSubmit
  };
}

export default formValidation;
