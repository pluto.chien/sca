import React from "react";
import { shallow } from "enzyme";
import App from "./App";

describe("App component", () => {
  let wrapper;
  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, "useState");
  useStateSpy.mockImplementation(init => [init, setState]);


  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("shallow renders without crashing", () => {
    shallow(<App />);
  });

  it("render snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });



  it("calls setStat when change the input", () => {
    const wrapper = shallow(<App />)
      .find("RegisterForm")
      .shallow();
    const input = wrapper.find('input[name="firstName"]');
    input.simulate("focus");
    input.simulate("change", { target: { name: "firstName", value: "Pluto" } });
    expect(setState).toHaveBeenCalledWith(true);

  });


});
