const express = require("express");
const path = require("path");
const axios = require("axios");

const bodyParser = require("body-parser");

const isDev = process.env.NODE_ENV !== "production";
const PORT = process.env.PORT || 5000;

const app = express();

// Priority serve any static files.
app.use(express.static(path.resolve(__dirname, "../react-ui/build")));
app.use(bodyParser.json());

// Answer API requests.
app.post("/signup", function(req, res) {
  const data = { data: { ...req.body } };
  const headers = {
    "Content-Type": "application/json",
    "x-auth": "react-test"
  };
  axios
    .post(
      "https://ckzvgrbymezqegu.form.io/reacttestform/submission",
      data,
      headers
    )
    .then(response => {
      console.log(response);
      res.set("Content-Type", "application/json");
      res.status(200).send();
    })
    .catch(error => {
      console.log(error);
      res.status(500).send("Unhandled exception :: Internal server error");
    });
});

// All remaining requests return the React app, so it can handle routing.
app.get("*", function(request, response) {
  response.sendFile(path.resolve(__dirname, "../react-ui/build", "index.html"));
});

app.listen(PORT, function() {
  console.error(
    `Node ${
      isDev ? "dev server" : "cluster worker " + process.pid
    }: listening on port ${PORT}`
  );
});
